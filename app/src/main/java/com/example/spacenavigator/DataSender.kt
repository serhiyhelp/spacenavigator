package com.example.spacenavigator

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import java.io.*

class DataSender : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_sender)

        populateList()

        intent.getStringExtra("address")?.let { btConnect(it) }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
            populateList()
    }

    fun onNewLocation(view: View){
        val intent = Intent(this, NewLocation::class.java)
        startActivityForResult(intent, 2)
    }

    private fun populateList() {
        val list = findViewById<LinearLayout>(R.id.location_list)
        list.removeAllViewsInLayout()

        getLocations(applicationContext)?.split("\n")?.forEach{
            val fields = it.split(" ")
            val btn = Button(this)
            btn.text = fields[0]
            btn.setOnClickListener{
                var intent = Intent(this, StarCoordinates::class.java)
                intent.putExtra("lon", fields[1].toDouble())
                intent.putExtra("lat", fields[2].toDouble())
                startActivity(intent)
            }

            list.addView(btn)
        }
    }

    private fun getLocations(context: Context): String? {
        var ret = ""
        try {
            val inputStream: InputStream? = context.openFileInput("locations")
            if (inputStream != null) {
                val inputStreamReader = InputStreamReader(inputStream)
                val bufferedReader = BufferedReader(inputStreamReader)
                var receiveString: String? = ""
                val stringBuilder = StringBuilder()
                while (bufferedReader.readLine().also({ receiveString = it }) != null) {
                    stringBuilder.append("\n").append(receiveString)
                }
                inputStream.close()
                ret = stringBuilder.toString()
            }
        } catch (e: FileNotFoundException) {
            Log.e("login activity", "File not found: " + e.toString())
        } catch (e: IOException) {
            Log.e("login activity", "Can not read file: $e")
        }
        return ret
    }
}