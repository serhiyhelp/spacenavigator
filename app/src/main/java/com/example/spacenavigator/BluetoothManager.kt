package com.example.spacenavigator

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import java.io.IOException

lateinit var btAdapter : BluetoothAdapter
lateinit var btSocket: BluetoothSocket

fun getBtDevices() : Set<BluetoothDevice> {
    btAdapter = BluetoothAdapter.getDefaultAdapter()
    return btAdapter.bondedDevices
}

fun btConnect(address: String) {
    val device = btAdapter.getRemoteDevice(address)
    val uuid = device.uuids[0].uuid
    btAdapter.cancelDiscovery()

    try {
        btSocket = device.createRfcommSocketToServiceRecord(uuid)
        btSocket.connect()
    } catch (e: IOException) {
        try {
            btSocket.close()
        } catch (e2: IOException) { }
    }
}

fun btWrite(data: String) {
    var outStream = btSocket.outputStream
    try {
        outStream = btSocket.outputStream
    } catch (e: IOException) { }
    val msgBuffer = data.toByteArray()
    try {
        outStream.write(msgBuffer)
    } catch (e: IOException) { }
}
