package com.example.spacenavigator

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationServices
import com.google.android.material.chip.Chip
import java.io.IOException
import java.io.OutputStreamWriter

class NewLocation : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_location)
    }


    fun onCancel(view: View){
        finish()
    }

    fun onSubmit(view: View){
        var lon = 0.0
        var lat = 0.0
        if (findViewById<Chip>(R.id.gps_check).isChecked){
            val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location : Location? ->
                    if (location != null) {
                        lon = location.longitude
                        lat = location.latitude
                    }
                }
            lat = 50.4500336
            lon = 30.5241361
        } else {
            lon = findViewById<EditText>(R.id.lon_input).text.toString().toDouble()
            lat = findViewById<EditText>(R.id.lat_input).text.toString().toDouble()
        }
        val name = findViewById<EditText>(R.id.name_input).text.toString()


        saveLocation("$name $lon $lat\n", this.applicationContext)

        setResult(0)
        finish()
    }
    private fun saveLocation(data: String, context: Context) {
        try {
            val outputStreamWriter =
                OutputStreamWriter(context.openFileOutput("locations", Context.MODE_APPEND))
            outputStreamWriter.write(data)
            outputStreamWriter.close()
        } catch (e: IOException) {
            Log.e("Exception", "File write failed: " + e.toString())
        }
    }

}