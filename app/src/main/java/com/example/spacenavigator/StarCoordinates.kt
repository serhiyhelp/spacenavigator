package com.example.spacenavigator

import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class StarCoordinates : AppCompatActivity() {
    var lat: Double = 0.0
    var lon: Double = 0.0
    var move: Boolean = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_star_coordinates)

        lat = intent.getDoubleExtra("lat", 0.0)
        lon = intent.getDoubleExtra("lon", 0.0)
    }

    fun onDirect(view: View){
        if (!move) {
            val h = findViewById<EditText>(R.id.h_input).text.toString().toDouble()
            val r = findViewById<EditText>(R.id.r_input).text.toString().toDouble()

            val day = 8640000
            val time = (Calendar.getInstance().timeInMillis % day) * (360 / day)

            val relativeH = (lon + h + time) % 360
            val relativeR = (90 - lat) + r
            val message = "$relativeH $relativeR"

            btWrite(message)
        }
    }
    fun onMoveToggle(view: View){
        move = !move
        if (move){
            btWrite("m\n")
        } else {
            btWrite("s\n")
        }
    }
}